unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm2 = class(TForm)
    Memo1: TMemo;
    Memo2: TMemo;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var c :String;
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
begin
 c:= Memo1.Text;
 Memo2.Text := c.Length.ToString;

end;

procedure TForm2.Button2Click(Sender: TObject);
begin
 c:= Memo1.Text;
 if c.Contains('s') then
   Begin
    Memo2.Text := ('Cont�m a letra S');
   End
 Else
   Begin
    Memo2.Text := ('N�o h� S');
   End;
end;

procedure TForm2.Button3Click(Sender: TObject);
begin
  c:= Memo1.Text;
  Memo2.Text := c.Trim;
end;

procedure TForm2.Button4Click(Sender: TObject);
begin
  c:= Memo1.Text;
  Memo2.Text := LowerCase(c);
end;

procedure TForm2.Button5Click(Sender: TObject);
begin
  c:= Memo1.Text;
  Memo2.Text := UpperCase(c);
end;

procedure TForm2.Button6Click(Sender: TObject);
begin
  c:= Memo1.Text;
  Memo2.Text := c.Replace('c','i');
end;

end.
